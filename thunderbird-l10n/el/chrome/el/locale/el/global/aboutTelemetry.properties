# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# LOCALIZATION NOTE(pageSubtitle):
# - %1$S is replaced by the value of the toolkit.telemetry.server_owner preference
# - %2$S is replaced by brandFullName
pageSubtitle = Αυτή η σελίδα εμφανίζει τα δεδομένα συμπεριφοράς και χρήσης των λειτουργιών που συλλέγονται από την Τηλεμετρία. Αυτές οι πληροφορίες υποβάλλονται ανώνυμα στο %1$S και μας βοηθούν να βελτιώσουμε τον %2$S.

# LOCALIZATION NOTE(homeExplanation):
# - %1$S is either telemetryEnabled or telemetryDisabled
# - %2$S is either extendedTelemetryEnabled or extendedTelemetryDisabled
homeExplanation = Η τηλεμετρία είναι %1$S και η εκτεταμένη τηλεμετρία είναι %2$S.
telemetryEnabled = ενεργή
telemetryDisabled = ανενεργή
extendedTelemetryEnabled = ενεργοποιημένη
extendedTelemetryDisabled = απενεργοποιημένη

# LOCALIZATION NOTE(settingsExplanation):
# - %1$S is either releaseData or prereleaseData
# - %2$S is either telemetryUploadEnabled or telemetryUploadDisabled
telemetryUploadEnabled = ενεργό
telemetryUploadDisabled = ανενεργό

# LOCALIZATION NOTE(pingDetails):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by namedPing
pingDetails = Κάθε κομμάτι πληροφορίας αποστέλλεται πακεταρισμένο στο “%1$S”. Αυτή τη στιγμή βλέπετε το ping %2$S.
# LOCALIZATION NOTE(namedPing):
# - %1$S is replaced by the ping localized timestamp, e.g. “2017/07/08 10:40:46”
# - %2$S is replaced by the ping name, e.g. “saved-session”
namedPing = %1$S, %2$S
# LOCALIZATION NOTE(pingDetailsCurrent):
# - %1$S is replaced by a link with pingExplanationLink as text
# - %2$S is replaced by currentPing
pingDetailsCurrent = Κάθε κομμάτι πληροφορίας αποστέλλεται πακεταρισμένο στο “%1$S”. Αυτή τη στιγμή βλέπετε το ping %2$S.
pingExplanationLink = pings
currentPing = τρέχουσα

# LOCALIZATION NOTE(filterPlaceholder): string used as a placeholder for the
# search field, %1$S is replaced by the section name from the structure of the
# ping. More info about it can be found here:
# https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
filterPlaceholder = Εύρεση στο %1$S
filterAllPlaceholder = Εύρεση σε όλες τις ενότητες

# LOCALIZATION NOTE(resultsForSearch): %1$S is replaced by the searched terms
resultsForSearch = Αποτελέσματα για “%1$S”
# LOCALIZATION NOTE(noSearchResults):
# - %1$S is replaced by the section name from the structure of the ping.
# More info about it can be found here: https://firefox-source-docs.mozilla.org/toolkit/components/telemetry/telemetry/data/main-ping.html
# - %2$S is replaced by the current text in the search input
noSearchResults = Συγγνώμη! Δεν υπάρχουν αποτελέσματα στην ενότητα “%1$S” για “%2$S”
# LOCALIZATION NOTE(noSearchResultsAll): %S is replaced by the searched terms
# LOCALIZATION NOTE(noDataToDisplay): %S is replaced by the section name.
# This message is displayed when a section is empty.
# LOCALIZATION NOTE(currentPingSidebar): used as a tooltip for the “current”
# ping title in the sidebar
currentPingSidebar = τρέχον ping
# LOCALIZATION NOTE(telemetryPingTypeAll): used in the “Ping Type” select
telemetryPingTypeAll = όλα

# LOCALIZATION NOTE(histogram*): these strings are used in the “Histograms” section
histogramSamples = δείγματα
histogramAverage = μέσος όρος
histogramSum = άθροισμα
# LOCALIZATION NOTE(histogramCopy): button label to copy the histogram
histogramCopy = Αντιγραφή

# LOCALIZATION NOTE(telemetryLog*): these strings are used in the “Telemetry Log” section
telemetryLogTitle = Αρχείο καταγραφής τηλεμετρίας
telemetryLogHeadingId = Id
telemetryLogHeadingTimestamp = Χρονική σήμανση
telemetryLogHeadingData = Δεδομένα

# LOCALIZATION NOTE(slowSql*): these strings are used in the “Slow SQL Statements” section
slowSqlMain = Εντολές αργής SQL στο κύριο νήμα
slowSqlOther = Εντολές αργής SQL στα νήματα βοήθειας
slowSqlHits = Συμβάντα
slowSqlAverage = Μέσος χρόνος (ms)
slowSqlStatement = Εντολή

# LOCALIZATION NOTE(histogram*): these strings are used in the “Add-on Details” section
addonTableID = ID προσθέτου
addonTableDetails = Λεπτομέρειες
# LOCALIZATION NOTE(addonProvider):
# - %1$S is replaced by the name of an Add-on Provider (e.g. “XPI”, “Plugin”)
addonProvider = Πάροχος %1$S

keysHeader = Ιδιότητα
namesHeader = Όνομα
valuesHeader = Τιμή

# LOCALIZATION NOTE(chrome-hangs-title):
# - %1$S is replaced by the number of the hang
# - %2$S is replaced by the duration of the hang
chrome-hangs-title = Αναφορά "κολλήματος" #%1$S (%2$S δευτερόλεπτα)
# LOCALIZATION NOTE(captured-stacks-title):
# - %1$S is replaced by the string key for this stack
# - %2$S is replaced by the number of times this stack was captured
captured-stacks-title = %1$S (μέτρηση καταλήψεων: %2$S)
# LOCALIZATION NOTE(late-writes-title):
# - %1$S is replaced by the number of the late write
late-writes-title = Καθυστερημένη εγγραφή #%1$S

stackTitle = Στοίβα:
memoryMapTitle = Χάρτης μνήμης:

errorFetchingSymbols = Προέκυψε σφάλμα κατά την λήψη συμβόλων. Βεβαιωθείτε ότι είστε συνδεδεμένοι στο διαδίκτυο και προσπαθήστε ξανά.

parentPayload = Γονικό ωφέλιμο φορτίο
# LOCALIZATION NOTE(childPayloadN):
# - %1$S is replaced by the number of the child payload (e.g. “1”, “2”)
childPayloadN = Θυγατρικό ωφέλιμο φορτίο %1$S
timestampHeader = χρονική σήμανση
categoryHeader = κατηγορία
methodHeader = μέθοδος
objectHeader = αντικείμενο
extraHeader = επιπλέον

settingsExplanation = Telemetry is collecting %1$S and upload is %2$S.
releaseData = release data
prereleaseData = pre-release data
noSearchResultsAll = Sorry! There are no results in any sections for “%S”
noDataToDisplay = Sorry! There is currently no data available in “%S”
