# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# The following are used by the Mailing list dialog.
# LOCALIZATION NOTE (mailingListTitleEdit): %S will be replaced by the Mailing List's display name.
mailingListTitleEdit=Editați %S
emptyListName=Introduceți un nume pentru listă.
lastFirstFormat=%S, %S
firstLastFormat=%S %S

allAddressBooks=Toate agendele

newContactTitle=Contact nou
# %S will be the contact's display name
newContactTitleWithDisplayName=Contact nou pentru %S
editContactTitle=Editare contact
# %S will be the contact's display name
editContactTitleWithDisplayName=Editează contactul pentru %S
# don't translate vCard
editVCardTitle=Editare vCard
# %S will be the card's display name, don't translate vCard
editVCardTitleWithDisplayName=Editare vCard pentru %S

## LOCALIZATION NOTE (cardRequiredDataMissingMessage):  do not localize \n
cardRequiredDataMissingMessage=Trebuie să introduceți măcar una din următoarele:\nAdresa de e-mail, Prenumele, Numele de familie, Numele afișat, Organizația.
cardRequiredDataMissingTitle=Lipsesc informații necesare
incorrectEmailAddressFormatMessage=Adresa primară de e-mail trebuie să fie de forma utilizator@gazdă.
incorrectEmailAddressFormatTitle=Forma adresei e-mail incorectă

viewListTitle=Listă de expediție: %S
mailListNameExistsTitle=Listă de expediție deja existentă
mailListNameExistsMessage=Există deja o listă de expediție cu acest nume. Vă rugăm să alegeți un nume diferit.

confirmDeleteThisContactTitle=Ștergere contact
# LOCALIZATION NOTE (confirmDeleteThisContact):
# #1 The name of the selected contact
# Don't localize "\n• #1" unless your local layout comes out wrong.
# Example: Are you sure you want to delete this contact?
#          • John Doe
confirmDeleteThisContact=Sigur doriți să ștergeți acest contact?\n• #1

confirmDelete2orMoreContactsTitle=Ștergere contacte multiple
# LOCALIZATION NOTE (confirmDelete2orMoreContacts):
# Semicolon list of plural forms.
# See: http://developer.mozilla.org/docs/Localization_and_Plurals
# #1 The number of selected contacts, always more than 1.
# Example: Are you sure you want to delete these 3 contacts?
confirmDelete2orMoreContacts=Sigur doriți să ștergeți acest contact?;Sigur doriți să ștergeți aceste #1 contacte?;Sigur doriți să ștergeți aceste #1 de contacte?

confirmRemoveThisContactTitle=Eliminare contact
# LOCALIZATION NOTE (confirmRemoveThisContact):
# #1 The name of the selected contact
# #2 The name of the containing mailing list
# This title is about a contact in a mailing list, so it will not be deleted,
# but only removed from the list.
# Don't localize "\n• #1" unless your local layout comes out wrong.
# Example: Are you sure you want to remove this contact from the mailing list 'Customers List'?
#          • John Doe
confirmRemoveThisContact=Sigur doriți să eliminați acest contact din lista de mail '#2'?\n• #1

confirmRemove2orMoreContactsTitle=Eliminare contacte multiple
# LOCALIZATION NOTE (confirmRemove2orMoreContacts):
# Semicolon list of singular and plural forms.
# See: http://developer.mozilla.org/docs/Localization_and_Plurals
# #1 The number of selected contacts, always more than 1.
# #2 The name of the containing mailing list
# Example: Are you sure you want to remove these 3 contacts from the mailing list 'Customers List'?
confirmRemove2orMoreContacts=Sigur doriți să eliminați acest contact din lista de mail „#2”?;Sigur doriți să eliminați aceste #1 contacte din lista de mail „#2”?;Sigur doriți să eliminați aceste #1 de contacte din lista de mail „#2”?

confirmDeleteThisMailingListTitle=Ștergere listă expediție
# LOCALIZATION NOTE (confirmDeleteThisMailingList):
# #1 The name of the selected mailing list
# Don't localize "\n• #1" unless your local layout comes out wrong.
# Example: Are you sure you want to delete this mailing list?
#          • Customers List
confirmDeleteThisMailingList=Sigur dorești să ștergi această listă de expediție?\n• #1

confirmDelete2orMoreMailingListsTitle=Șterge mai multe liste de e-mail
# LOCALIZATION NOTE (confirmDelete2orMoreMailingLists):
# Semicolon list of plural forms.
# See: http://developer.mozilla.org/docs/Localization_and_Plurals
# #1 The number of selected mailing lists, always more than 1
# Example: Are you sure you want to delete these 3 mailing lists?
confirmDelete2orMoreMailingLists=Sigur doriți să ștergeți această listă de mail?;Sigur doriți să ștergeți aceste #1 liste de mail?;Sigur doriți să ștergeți aceste #1 de liste de mail?

confirmDelete2orMoreContactsAndListsTitle=Ștergere contacte și liste de e-mail
# LOCALIZATION NOTE (confirmDelete2orMoreContactsAndLists):
# Semicolon list of and plural forms.
# See: http://developer.mozilla.org/docs/Localization_and_Plurals
# #1 The number of selected contacts and mailing lists, always more than 1
# Example: Are you sure you want to delete these 3 contacts and mailing lists?
confirmDelete2orMoreContactsAndLists=Sigur doriți să ștergeți acest contact ori listă de e-mail?;Sigur doriți să ștergeți aceste #1 contacte sau liste de e-mail?;Sigur doriți să ștergeți aceste #1 de contacte sau liste de e-mail?

confirmDeleteThisAddressbookTitle=Șterge agenda de contacte
# LOCALIZATION NOTE (confirmDeleteThisAddressbookTitle):
# #1 The name of the selected address book
# Don't localize "\n• #1" unless your local layout comes out wrong.
# Example: Are you sure you want to delete this address book and all of its contacts?
#          • Friends and Family Address Book
confirmDeleteThisAddressbook=Sigur doriți să ștergeți această agendă și toate contactele ei?\n• #1

confirmDeleteThisLDAPDirTitle=Șterge directorul LDAP local
# LOCALIZATION NOTE (confirmDeleteThisLDAPDir):
# #1 The name of the selected LDAP directory
# Don't localize "\n• #1" unless your local layout comes out wrong.
# Example: Are you sure you want to delete the local copy of this LDAP directory and all of its offline contacts?
#          • Mozilla LDAP Directory
confirmDeleteThisLDAPDir=Sigur doriți să ștergeți copia locală a directorului LDAP și toate contactele sale salvate?\n• #1

confirmDeleteThisCollectionAddressbookTitle=Ștergere agendă de contacte colectate
# LOCALIZATION NOTE (confirmDeleteThisCollectionAddressbook):
# #1 The name of the selected collection address book
# #2 The name of the application (Thunderbird)
# Don't localize "\n• #1" unless your local layout comes out wrong.
# Example: If this address book is deleted, Thunderbird will no longer collect addresses.
#          Are you sure you want to delete this address book and all of its contacts?
#          • My Collecting Addressbook
confirmDeleteThisCollectionAddressbook=Dacă ștergeți această agendă de contacte, nu se vor mai colecta adrese în #2.\nSigur doriți ștergerea acestei agende și a contactelor sale?\n• #1

propertyPrimaryEmail=E-mail
propertyListName=Nume listă
propertySecondaryEmail=E-mail suplimentar
propertyNickname=Pseudonim
propertyDisplayName=Nume de afișat
propertyWork=Serviciu
propertyHome=Acasă
propertyFax=Nr. de fax
propertyCellular=Mobil
propertyPager=Nr. de pager
propertyBirthday=Zi de naștere
propertyCustom1=Diverse 1
propertyCustom2=Diverse 2
propertyCustom3=Diverse 3
propertyCustom4=Diverse 4

propertyGtalk=Google Talk
propertyAIM=AIM
propertyYahoo=Yahoo!
propertySkype=Skype
propertyQQ=QQ
propertyMSN=MSN
propertyICQ=ICQ
propertyXMPP=ID Jabber
propertyIRC=Pseudonim IRC

## LOCALIZATION NOTE (cityAndStateAndZip):
## %1$S is city, %2$S is state, %3$S is zip
cityAndStateAndZip=%1$S, %2$S %3$S
## LOCALIZATION NOTE (cityAndStateNoZip):
## %1$S is city, %2$S is state
cityAndStateNoZip=%1$S, %2$S
## LOCALIZATION NOTE (cityOrStateAndZip):
## %1$S is city or state, %2$S is zip
cityOrStateAndZip=%1$S %2$S

stateZipSeparator=

prefixTo=Către
prefixCc=Cc
prefixBcc=Bcc
addressBook=Agendă de contacte

# Contact photo management
browsePhoto=Poză de contact

stateImageSave=Imaginea se salvează…
errorInvalidUri=Eroare: imaginea sursă este nevalidă.
errorNotAvailable=Eroare: fișierul nu poate fi accesat.
errorInvalidImage=Eroare: Sunt permise doar imagini de tipul JPG, PNG sau GIF.
errorSaveOperation=Eroare: Imaginea nu a putut fi salvată.

# mailnews.js
ldap_2.servers.pab.description=Agendă personală
ldap_2.servers.history.description=Adrese adunate
## LOCALIZATION NOTE (ldap_2.servers.osx.description is only used on Mac OS X)
ldap_2.servers.osx.description=Agendă de contacte Mac OS X

# status bar stuff
## LOCALIZATION NOTE (totalContactStatus):
## %1$S is address book name, %2$S is contact count
totalContactStatus=Total contacte în %1$S: %2$S
noMatchFound=Nicio fișă găsită
## LOCALIZATION NOTE (matchesFound1):
## Semicolon-separated list of singular and plural forms.
## See: https://developer.mozilla.org/docs/Mozilla/Localization/Localization_and_Plurals
## #1 is the number of matching contacts found
matchesFound1=#1 rezultat găsit;#1 rezultate găsite;#1 de rezultate găsite

## LOCALIZATION NOTE (contactsCopied): Semi-colon list of plural forms
## %1$S is the number of contacts that were copied. This should be used multiple
## times wherever you need it. Do not replace by %S.
contactsCopied=%1$S contact copiat;%1$S contacte copiate

## LOCALIZATION NOTE (contactsMoved): Semi-colon list of plural forms
## %1$S is the number of contacts that were moved. This should be used multiple
## times wherever you need it. Do not replace by %S.
contactsMoved=%1$S contact mutat;%1$S contacte mutate

# LDAP directory stuff
invalidName=Vă rugăm să introduceți un nume valid.
invalidHostname=Te rugăm să introduci un nume de gazdă corect.
invalidPortNumber=Vă rugăm să introduceți un număr de port valid.
invalidResults=Vă rugăm să introduceți un număr corect în câmpul rezultate.
abReplicationOfflineWarning=Trebuie să fiți conectat(ă) pentru a efectua replicare LDAP
abReplicationSaveSettings=Trebuie să salvați setările înainte să puteți descărca un director.

# For importing / exporting
## LOCALIZATION NOTE (ExportAddressBookNameTitle): %S is the name of exported addressbook
ExportAddressBookNameTitle=Exportă agendă de contacte - %S
LDIFFiles=LDIF
CSVFiles=Delimitat de virgule
CSVFilesSysCharset=Delimita de virgule (System Charset)
CSVFilesUTF8=Separate prin virgulă (UTF-8)
TABFiles=Delimitat de taburi
TABFilesSysCharset=Separate prin tab (set de caractere de sistem)
TABFilesUTF8=Separate prin tab (UTF-8)
VCFFiles=vCard
failedToExportTitle=Export eșuat
failedToExportMessageNoDeviceSpace=Eșec la exportul agendei de contacte, nu mai e spațiu liber pe dispozitiv.
failedToExportMessageFileAccessDenied=Eșec la exportul agendei de contacte, acces interzis la fișier.

# For getting authDN for replication using dlg box
AuthDlgTitle=Copiere agendă de contacte LDAP
AuthDlgDesc=Pentru accesul la serverul LDAP, introduceți numele și parola.

# LOCALIZATION NOTE(joinMeInThisChat)
# use + for spaces
joinMeInThisChat=Vino+cu+mine+în+chat

# For printing
headingHome=Acasă
headingWork=Serviciu
headingOther=Altele
headingChat=Chat
headingPhone=Telefon
headingDescription=Descriere
headingAddresses=Adrese

## For address books
addressBookTitleNew=Agendă de contacte nouă
# LOCALIZATION NOTE (addressBookTitleEdit):
# %S is the current name of the address book.
# Example: My Custom AB Properties
addressBookTitleEdit=Proprietăți pentru %S
duplicateNameTitle=Nume duplicat de agendă de contacte
# LOCALIZATION NOTE (duplicateNameText):
# Don't localize "\n• %S" unless your local layout comes out wrong.
# %S is the name of the existing address book.
# Example: An address book with this name already exists:
#          • My Custom AB
duplicateNameText=O agendă de contacte cu acest nume există deja:\n• %S

# For corrupt .mab files
corruptMabFileTitle=Fișier cu agendă de contacte corupt
corruptMabFileAlert=Unul din fișierele cu agende de contacte (%1$S) nu poate fi citit. Se va crea un fișier nou %2$S și o copie de siguranță a vechiului fișier, cu numele %3$S, în același director.

# For locked .mab files
lockedMabFileTitle=Nu se poate încărca fișierul cu agenda de contacte
lockedMabFileAlert=Fișierul cu agenda de contacte %S nu poate fi încărcat. Ar putea fi din cauza faptului că fișierul poate fi doar citit sau este blocat de altă aplicație. Vă rugăm să încercați din nou mai târziu.
